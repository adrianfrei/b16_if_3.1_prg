package ch.adifrei.abbts.prg.week5.numbers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class under test: {@link BitStringConverter}
 * 
 * @author adi
 *
 */
public class BitStringConverterTest {

	BitStringConverter converter;

	@BeforeEach
	public void setUp() {
		converter = new BitStringConverter();
	}

	@Test
	public void testConvert() {
		String in = "00000000000000000000000000000000";
		assertEquals(0, converter.convert(in));
	}
	@Test
	public void testConvert1() {
		String in = "00000000000000000000000000000001";
		assertEquals(1, converter.convert(in));
	}
	@Test
	public void testConvertMin712() {
		String in = "11111111111111111111110100111000";
		assertEquals(-712, converter.convert(in));
	}
	@Test
	public void testConvert1328() {
		String in = "000000000000000000000010100110000";
		assertEquals(1328, converter.convert(in));
	}
}
