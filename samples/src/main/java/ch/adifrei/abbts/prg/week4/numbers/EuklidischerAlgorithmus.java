package ch.adifrei.abbts.prg.week4.numbers;

import java.util.Scanner;

/**
 * 
 * @author adi
 *
 */
public class EuklidischerAlgorithmus {
	public static void main(String[] args) {
		int aStart = 0;
		int bStart = 0;
		int a = 0;
		int b = 0;
		int h = 0;
		boolean abbruch = false;

		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter("[;\r\n]+");
		while (!abbruch) {
			while (a == 0) {
				System.out.println("Geben Sie die Zahl a ein");
				if (scanner.hasNextInt()) {
					a = scanner.nextInt();
					aStart = a;
				} else {
					scanner.next();
				}
			}
			while (b == 0) {
				System.out.println("Geben Sie die Zahl b ein");
				if (scanner.hasNextInt()) {
					b = scanner.nextInt();
					bStart = b;
				} else {
					scanner.next();
				}
			}
			while (b != 0) {
				h = a % b;
				a = b;
				b = h;
			}
			System.out.println("ggT(" + aStart + "," + bStart + ")=" + a);
			System.out.println("Geben Sie x ein um abzubrechen");
			a = 0;
			b = 0;
			if (scanner.next().equalsIgnoreCase("x")) {
				abbruch = true;
			} else {
				abbruch = false;
			}
		}
	}
}
