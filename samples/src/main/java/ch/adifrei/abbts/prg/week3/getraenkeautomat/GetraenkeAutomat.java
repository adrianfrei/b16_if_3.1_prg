package ch.adifrei.abbts.prg.week3.getraenkeautomat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Scanner;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;

import org.javamoney.moneta.Money;
import org.javamoney.moneta.format.CurrencyStyle;

/**
 * Getraenke Automat von Adrian Frei vom 10.11.2017
 * 
 * Verwendet wurde die Java-Money-API (JSR-354), da Betraege von Waehrungen
 * immer an eine Waehrung gekoppelt sein muessen, damit sie eine Aussage (Wert)
 * haben.
 * 
 * @author adi
 *
 */

public class GetraenkeAutomat {

	// Format um die Ausgaben zu formatieren
	private static MonetaryAmountFormat customFormat = //
			MonetaryFormats.getAmountFormat(//
					AmountFormatQueryBuilder.of(Locale.GERMAN)//
							.set(CurrencyStyle.CODE)//
							.set("pattern", "0.00 ¤")//
							.build());

	// Definition der zu verwendenden Waehrung
	private static CurrencyUnit chf = Monetary.getCurrency("CHF");

	// Preise der Artikel
	private static final MonetaryAmount KAFFEE_PREIS = //
			Money.of(1.0, chf);
	private static final MonetaryAmount OVO_PREIS = //
			Money.of(1.1, chf);
	private static final MonetaryAmount TEE_PREIS = //
			Money.of(0.8, chf);

	// Alle moeglichen Muenzen des Schweizer Frankens
	private static Collection<MonetaryAmount> COINS = Arrays.asList(//
			Money.of(5, chf), //
			Money.of(2, chf), //
			Money.of(1, chf), //
			Money.of(0.5, chf), //
			Money.of(0.2, chf), //
			Money.of(0.1, chf) //
	);

	private static final Integer OPTION_EINWURF = 1;
	private static final Integer OPTION_KAFFEE = 2;
	private static final Integer OPTION_OVO = 3;
	private static final Integer OPTION_TEE = 4;
	private static final Integer OPTION_RETOUR_GELD = 5;
	private static final Integer OPTION_BEENDEN = 6;

	private static Double wasserstand = 5.0d;

	private static MonetaryAmount kredit = Money.of(0, chf);

	// Menuetext
	private static final String MENUE = //
			"-------- MENUE --------\n" //
					+ "Muenze einwerfen....." + OPTION_EINWURF + "\n" //
					+ "Kaffee (" //
					+ customFormat.format(KAFFEE_PREIS) + ")...." + OPTION_KAFFEE + "\n" //
					+ "Ovo (" //
					+ customFormat.format(OVO_PREIS) + ")......." + OPTION_OVO + "\n"//
					+ "Tee ("//
					+ customFormat.format(TEE_PREIS) + ")......." + OPTION_TEE + "\n"//
					+ "Retour-Geld.........." + OPTION_RETOUR_GELD + "\n"//
					+ "Beenden.............." + OPTION_BEENDEN;

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Integer option = null;
		while (option == null || !option.equals(OPTION_BEENDEN)) {
			option = menu();
			if (option.equals(OPTION_EINWURF)) {
				kredit = kredit.add(muenzEinwurf());
				System.out.println("Neuer Kredit " + customFormat.format(kredit));
			} else if (option.equals(OPTION_KAFFEE) || option.equals(OPTION_TEE)) {
				Boolean enoughWater = hasEnoughWater();
				if (enoughWater) {
					if (option.equals(OPTION_KAFFEE)) {
						make("Kaffee", 0.2);
						kredit = pay(KAFFEE_PREIS, kredit);
					} else if (option.equals(OPTION_TEE)) {
						make("Tee", 0.2);
						kredit = pay(TEE_PREIS, kredit);
					}
				} else {
					System.out.println("Wasserstand zu klein");
					end();
				}
			} else if (option.equals(OPTION_OVO)) {
				if (hasEnoughCredit(OVO_PREIS)) {
					make("Ovo");
					kredit = pay(OVO_PREIS, kredit);
				} else {
					System.out.println("Kredit ist zu klein");
				}

			} else if (option.equals(OPTION_RETOUR_GELD)) {
				retourGeldAuszahlen();
			} else if (option.equals(OPTION_BEENDEN)) {
				end();
			} else {
				System.out.println("Ungueltige Operation");
			}
		}

	}

	private static void make(String drink, Double benoetigtesWasser) {
		wasserstand -= benoetigtesWasser;
		System.out.println("Neuer Waaserstand: " + wasserstand);
		make(drink);
	}

	private static MonetaryAmount pay(MonetaryAmount preis, MonetaryAmount kredit) {
		kredit = kredit.subtract(preis);
		return kredit;
	}

	private static void make(String drink) {
		System.out.println(drink + " wird zubereitet");
	}

	private static void end() {
		scanner.close();
		retourGeldAuszahlen();
		System.out.println("Bye bye!");
		System.exit(0);
	}

	private static void retourGeldAuszahlen() {
		for (MonetaryAmount coin : COINS) {
			while (coin.isLessThanOrEqualTo(kredit)) {
				System.out.println("Ihr Rueckgeld " + customFormat.format(coin));
				kredit = kredit.subtract(coin);
			}
		}
	}

	private static Boolean hasEnoughCredit(MonetaryAmount preis) {
		return kredit.isGreaterThanOrEqualTo(preis);
	}

	private static Boolean hasEnoughWater() {
		return wasserstand >= 2;
	}

	private static Integer menu() {
		System.out.println(MENUE);
		System.out.println("Ihre Wahl:");
		Integer option = scanner.nextInt();
		System.out.println("Ihre Wahl war: " + option);
		return option;
	}

	private static MonetaryAmount muenzEinwurf() {
		MonetaryAmount addedAmount = Money.of(0, chf);
		Boolean coinInserted = Boolean.valueOf(false);
		Boolean validCoin = Boolean.valueOf(false);
		while (!coinInserted && !validCoin) {
			System.out.println("Bitte geben Sie den Betrag ein");
			Double amount = scanner.nextDouble();
			MonetaryAmount inserted = Money.of(amount, chf);
			if (!COINS.contains(inserted)) {
				validCoin = Boolean.valueOf(false);
				System.out.println("Bitte werfen Sie eine gueltige Muenze ein");
			} else {
				addedAmount = addedAmount.add(inserted);
				validCoin = Boolean.valueOf(true);
			}
		}
		return addedAmount;
	}

}
