package ch.adifrei.abbts.prg.week4.numbers;

import java.util.Scanner;

/**
 * 
 * @author adi
 *
 */
public class Fibonacci {
	public static void main(String[] args) {

		System.out.println("Geben Sie die Anzahl Durchgaenge (n) ein");
		int n = new Scanner(System.in).nextInt();

		long current = 0;
		long before1 = 1;
		long before2 = 0;

		for (int i = 1; i < n; i++) {
			current = before1 + before2;
			before2 = before1;
			before1 = current;
			System.out.println(current);
		}
	}
}
