package ch.adifrei.abbts.prg.week4.numbers;

public class Sum {

	public static void main(String[] args) {

		final int MAX = 12;

		final int excludedDivisor = 5;

		int sum = 0;

		int i = 0;

		int count = 0;

		while (count < MAX) {
			if (i % 2 == 1 && !(i % excludedDivisor == 0)) {

				sum += i;
				count++;
			}
			i++;
		}
		System.out.println(sum);
	}

}
