package ch.adifrei.abbts.prg.week5.numbers;

import java.util.Scanner;

/**
 * Bitstring Converter fuer die Einfuehrung in die Programmierung.
 * 
 * @author adi
 *
 */
public class BitStringConverter {

	public static void main(String[] args) {
		final int LENGHT = 32;
		while (true) {
			System.out.println("Geben Sie den 32-Stelligen Bit-String ein:");
			String bits = new Scanner(System.in).nextLine();
			if (bits.length() == LENGHT) {
				System.out.println("Resultat: " + convert(bits));
			} else {
				System.out.println("Laenge der Eingabe ist nicht 32");
			}
		}
	}

	/**
	 * Die Methode convert wurde extrahiert, damit Unit-Tests gemacht werden
	 * koennen.
	 * 
	 * @param bits Der 32-stellige binäre String
	 * @return eine positive oder negative Ganzzahl
	 */
	public static int convert(String bits) {
		int num = 0;
		boolean negative = false;
		if (Character.getNumericValue(bits.charAt(0)) == 1) {
			negative = true;
		}
		for (int i = 1; i < bits.length() - 1; i++) {
			int bit = Character.getNumericValue(bits.charAt(bits.length() - i));
			if (negative) {
				bit = flipBit(bit);
			}
			num += bit * Math.pow(2, i - 1);
		}
		if (negative) {
			num++;
			num = num * -1;
		}
		return num;
	}

	private static int flipBit(int bit) {
		if (bit == 1) {
			bit = 0;
		} else if (bit == 0) {
			bit = 1;
		}
		return bit;
	}
}
