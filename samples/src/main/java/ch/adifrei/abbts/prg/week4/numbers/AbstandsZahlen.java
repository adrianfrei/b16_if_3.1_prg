package ch.adifrei.abbts.prg.week4.numbers;

/**
 * 
 * @author adi
 */
public class AbstandsZahlen {

	public static void main(String[] args) {
		int countAbstandsZahlenSmallerThanN = 0;
		int abstand = 1;
		int zahl = 1;
		int maximum = 40;
		int n = 4;
		int untereGrenze = 3; // aka r
		int obereGrenze = 30; // aka s
		int count = 0;

		while (zahl < maximum) {
			if (abstand < n) {
				countAbstandsZahlenSmallerThanN++;
			}
			if (abstand <= obereGrenze && abstand >= untereGrenze) {
				count++;
			}
			zahl += abstand;
			abstand += 1;
		}
		System.out.println(countAbstandsZahlenSmallerThanN + " Abstandszahlen sind kleiner als " + n);
		System.out.println(count + " Abstandszahlen liegen sind groesser als " + untereGrenze + " und kleiner gleich "
				+ obereGrenze);
	}
}
