package ch.adifrei.abbts.prg.week1.helloworld;

/**
 * A Hello World example for ABBTS
 * 
 * Compiled with: javac -d C:\Users\adi\git\b16_if_3.1_prg\samples\target\classes 
 * -classpath C:\Users\adi\git\b16_if_3.1_prg\samples\target\classes;
 *  -sourcepath C:\Users\adi\git\b16_if_3.1_prg\samples\src\main\java;
 *  C:\Users\adi\git\b16_if_3.1_prg\samples\target\generated-sources\annotations;
 *  C:\Users\adi\git\b16_if_3.1_prg\samples\src\main\java\ch\adifrei\abbts\prg\week1
 *  \helloworld\HelloWorld.java 
 *  C:\Users\adi\git\b16_if_3.1_prg\samples\src\main\java\module-info.java 
 *  -s C:\Users\adi\git\b16_if_3.1_prg\samples\target\generated-sources\annotations 
 *  -g -nowarn --release 9 -encoding UTF-8
 * 
 * @author adi frei
 *
 */

public class HelloWorld {
	public static void main(String[] args) {
		System.out.println("Hello World");
	}
}
