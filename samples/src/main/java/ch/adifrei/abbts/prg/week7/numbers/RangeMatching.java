package ch.adifrei.abbts.prg.week7.numbers;

/**
 * Range Matching 
 * 
 * @author Adrian Frei
 *
 */
public class RangeMatching {
	public static void main(String[] args) {
		int[] arr = { 6, 8, 99, 3, 1, 0};
		int mi = 2;
		int ma = 7;
		int miFound= 0;
		int maFound = Integer.MAX_VALUE;
		for (int i : arr) {
			if (i<=mi && i > miFound) {
				miFound = i;
			}
			if (i >= ma && i < maFound) {
				maFound = i;
			}
		}
		System.out.println("int miFound = "+ miFound+ ", int maFound = "+ maFound);
	}
}
