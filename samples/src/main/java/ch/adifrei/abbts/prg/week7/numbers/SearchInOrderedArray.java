package ch.adifrei.abbts.prg.week7.numbers;

import java.util.Arrays;

/**
 * Binaere Suche
 * 
 * @author Adrian Frei
 *
 */
public class SearchInOrderedArray {

	public static void main(String[] args) {

		int searchTerm = 5400;

		// Neues Array initialisieren
		int[] plz = new int[100];
		// 100 Random Werte verwenden
		for (int i = 0; i < 100; i++) {
			int j = (int) (Math.random() * 10000);
			plz[i] = j;
		}
		// Naechste Zeile auskommentieren, damit der Wert sicher vorhanden ist.
		// plz[99] = 5400;
		// Array sortieren
		Arrays.sort(plz);

		boolean found = false;
		int firstSearchPosition = 0;
		int lastSearchPosition = plz.length;
		while (!found && (lastSearchPosition - firstSearchPosition) > 1) {
			int searchPos = firstSearchPosition + ((lastSearchPosition - firstSearchPosition) / 2);
			if (plz[searchPos] == searchTerm) {
				found = true;
				System.out.println(plz[searchPos] + " an Position " + searchPos + " gefunden");
			} else if (plz[searchPos] < searchTerm) {
				firstSearchPosition = searchPos;
			} else if (plz[searchPos] > searchTerm) {
				lastSearchPosition = searchPos;
			}
		}
		if (!found) {
			System.out.println("Zahl " + searchTerm + " wurde im Array nicht gefunden");
		}
	}
}
